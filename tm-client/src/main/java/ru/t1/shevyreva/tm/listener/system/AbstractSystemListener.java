package ru.t1.shevyreva.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.api.endpoint.ISystemEndpoint;
import ru.t1.shevyreva.tm.api.service.IPropertyService;
import ru.t1.shevyreva.tm.listener.AbstractListener;
import ru.t1.shevyreva.tm.enumerated.Role;

@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    public ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    AbstractListener[] listeners;

    @Nullable
    public IPropertyService getPropertyService() {
        if (getServiceLocator() == null) return null;
        return getServiceLocator().getPropertyService();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
