package ru.t1.shevyreva.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.api.endpoint.*;
import ru.t1.shevyreva.tm.api.repository.ICommandRepository;
import ru.t1.shevyreva.tm.api.service.*;
import ru.t1.shevyreva.tm.listener.AbstractListener;
import ru.t1.shevyreva.tm.event.ConsoleEvent;
import ru.t1.shevyreva.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.shevyreva.tm.exception.system.CommandNotSupportedException;
import ru.t1.shevyreva.tm.util.SystemUtil;
import ru.t1.shevyreva.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
@NoArgsConstructor
public class Bootstrap implements IServiceLocator {

    @NotNull
    @Getter
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Getter
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Getter
    @Autowired
    private ITokenService tokenService;

    @NotNull
    @Getter
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @NotNull
    @Getter
    @Autowired
    private IAuthEndpoint authEndpoint;

    @NotNull
    @Getter
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Getter
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @NotNull
    @Getter
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Getter
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @Nullable
    @Autowired
    private AbstractListener[] abstractListeners;

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    public void start(@Nullable final String[] args) {
        if (processArguments(args)) System.exit(0);

        prepareStartup();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("Enter command:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                loggerService.command(command);
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println(e.getMessage());
                System.out.println("[FAILED]");
            }
        }
    }

    public boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        processArgument(args[0]);
        return true;
    }

    public void processArgument(@Nullable final String arg) {
        if (arg == null) throw new ArgumentNotSupportedException(arg);
        publisher.publishEvent(new ConsoleEvent(arg));
    }

    public void processCommand(@Nullable final String command) {
        if (command == null) throw new CommandNotSupportedException(command);
        publisher.publishEvent(new ConsoleEvent(command));
    }

    private void prepareStartup() {
        loggerService.info("**Welcome to Task Manager**");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        initPID();
    }

    private void prepareShutdown() {
        loggerService.info("**Shutdown Task Manager**");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task_manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull File file = new File(fileName);
        file.deleteOnExit();
    }

}
