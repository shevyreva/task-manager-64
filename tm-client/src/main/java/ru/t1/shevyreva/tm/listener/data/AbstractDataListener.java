package ru.t1.shevyreva.tm.listener.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.api.endpoint.IDomainEndpoint;
import ru.t1.shevyreva.tm.listener.AbstractListener;

@Component
@NoArgsConstructor
public abstract class AbstractDataListener extends AbstractListener {

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_XML = "./data.xml";

    @NotNull
    public static final String FILE_JSON = "./data.json";

    @NotNull
    public static final String FILE_YAML = "./data.yaml";

    @NotNull
    public final String CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    public final String CONTEXT_FACTORY_JAXB = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    protected final String MEDIA_TYPE = "eclipselink.media-type";

    @NotNull
    protected final String APPLICATION_TYPE_JSON = "application/json";

    @NotNull
    @Autowired
    public IDomainEndpoint domainEndpoint;

}
