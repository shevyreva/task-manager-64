package ru.t1.shevyreva.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.request.task.TaskCreateRequest;
import ru.t1.shevyreva.tm.event.ConsoleEvent;
import ru.t1.shevyreva.tm.util.TerminalUtil;

@Component
public class TaskCreateListener extends AbstractTaskListener {

    @NotNull
    private final String DESCRIPTION = "Create new task.";

    @NotNull
    private final String NAME = "task-create";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskCreateListener.getName() == #consoleEvent.name")
    public @Nullable void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[CREATE TASK]");
        System.out.println("Enter name: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description: ");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(getToken(), name, description);
        taskEndpoint.createTask(request);
    }

}
