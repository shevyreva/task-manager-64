package ru.t1.shevyreva.tm.service;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.shevyreva.tm.api.service.IPropertyService;

import java.util.Properties;
@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @NotNull
    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @NotNull
    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @NotNull
    @Value("#{environment['server.port']}")
    private String serverPort;

    @NotNull
    @Value("#{environment['author.name']}")
    private String authorName;

    @NotNull
    @Value("#{environment['author.name']}")
    private String authorEmail;

    @NotNull
    @Value("#{environment['server.host']}")
    private String serverHost;

    @NotNull
    @Value("#{environment['application.version']}")
    private String applicationVersion;

}
