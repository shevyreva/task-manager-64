package ru.t1.shevyreva.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.request.project.ProjectClearRequest;
import ru.t1.shevyreva.tm.event.ConsoleEvent;

@Component
public class ProjectClearListener extends AbstractProjectListener {

    @NotNull
    private final String DESCRIPTION = "Clear project.";

    @NotNull
    private final String NAME = "project-clear";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectClearListener.getName() == #consoleEvent.name")
    public void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[CLEAR ALL PROJECTS]");
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getToken());
        projectEndpoint.clearProject(request);
    }

}
