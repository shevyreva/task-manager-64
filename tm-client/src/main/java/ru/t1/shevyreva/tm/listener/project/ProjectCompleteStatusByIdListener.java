package ru.t1.shevyreva.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.event.ConsoleEvent;
import ru.t1.shevyreva.tm.util.TerminalUtil;

@Component
public class ProjectCompleteStatusByIdListener extends AbstractProjectListener {

    @NotNull
    private final String DESCRIPTION = "Complete project by Id.";

    @NotNull
    private final String NAME = "project-update-status-by-id";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectCompleteStatusByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("Enter Id:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken(), id, Status.COMPLETED);
        projectEndpoint.changeProjectStatusById(request);
    }

}
