package ru.t1.shevyreva.tm.api;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jms.MessageListener;

public interface IReceiverService {

    void receive(@NotNull @Autowired MessageListener listener);

}
