package ru.t1.shevyreva.tm.component;


import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.listener.EntityListener;
import ru.t1.shevyreva.tm.service.ReceiverService;

@Component
public class Bootstrap {

    @NotNull
    @Autowired
    ReceiverService receiverService;

    @NotNull
    @Autowired
    private EntityListener entityListener;

    @SneakyThrows
    public void start() {
        receiverService.receive(entityListener);
    }

}
