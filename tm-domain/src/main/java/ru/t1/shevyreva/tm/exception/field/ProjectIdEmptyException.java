package ru.t1.shevyreva.tm.exception.field;

public final class ProjectIdEmptyException extends AbsrtactFieldException {

    public ProjectIdEmptyException() {
        super("Error! Project id is empty!");
    }

}
