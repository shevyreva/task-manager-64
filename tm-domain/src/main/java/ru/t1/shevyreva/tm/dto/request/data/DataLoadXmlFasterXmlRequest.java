package ru.t1.shevyreva.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;

public class DataLoadXmlFasterXmlRequest extends AbstractUserRequest {

    public DataLoadXmlFasterXmlRequest(@Nullable String token) {
        super(token);
    }

}
