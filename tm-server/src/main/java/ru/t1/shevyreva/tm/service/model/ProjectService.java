package ru.t1.shevyreva.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shevyreva.tm.api.service.model.IProjectService;
import ru.t1.shevyreva.tm.enumerated.ProjectSort;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.StatusNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.TaskNotFoundException;
import ru.t1.shevyreva.tm.exception.field.DescriptionEmptyException;
import ru.t1.shevyreva.tm.exception.field.IdEmptyException;
import ru.t1.shevyreva.tm.exception.field.NameEmptyException;
import ru.t1.shevyreva.tm.exception.field.UserIdEmptyException;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.repository.model.ProjectRepository;

import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
public final class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();

        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);

        add(userId, project);
        return project;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();

        @NotNull final Project project = new Project();
        project.setName(name);

        add(userId, project);
        return project;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);

        projectRepository.saveAndFlush(project);
        return project;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final Project project = findOneById(userId, id);
        project.setStatus(status);
        if (project == null) throw new ProjectNotFoundException();

        projectRepository.saveAndFlush(project);
        return project;
    }

    @Nullable
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId, @Nullable final ProjectSort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        if (sort == null) return projectRepository.findByUserId(userId);
        if (sort.getDisplayName() != null)
            return projectRepository.findAllByUserIdWithSort(userId, sort.name());
        else return null;
    }

    @NotNull
    @SneakyThrows
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable final Project project = projectRepository.findOneByUserIdAndId(userId, id);
        if (project == null) throw new TaskNotFoundException();
        return project;
    }

    @NotNull
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        return projectRepository.countByUserId(userId);
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Project removeOne(@Nullable final String userId, @Nullable final Project project) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ModelNotFoundException();

        projectRepository.deleteOneByIdAndUserId(project.getId(), userId);
        return project;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Project removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        Project project = projectRepository.findOneByUserIdAndId(userId, id);
        projectRepository.deleteOneByIdAndUserId(id, userId);
        return project;
    }

    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        projectRepository.deleteByUserId(userId);
    }

    @SneakyThrows
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Collection<Project> add(@NotNull final Collection<Project> models) {
        if (models == null) throw new ProjectNotFoundException();

        for (@NotNull Project project : models) {
            add(project);
        }
        return models;
    }

    @SneakyThrows
    @Transactional
    public void removeAll() {
        projectRepository.deleteAll();
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Collection<Project> set(@NotNull final Collection<Project> models) {
        @Nullable final Collection<Project> entities;
        removeAll();
        entities = add(models);
        return entities;
    }

    @SneakyThrows
    @Transactional
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return (projectRepository.findOneByUserIdAndId(userId, id) != null);
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Project add(@NotNull final Project model) {
        if (model == null) throw new ProjectNotFoundException();

        projectRepository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Project add(@NotNull final String userId, @NotNull final Project model) {
        if (model == null) throw new ProjectNotFoundException();

        //model.setUserId(userId);
        projectRepository.saveAndFlush(model);
        return model;
    }

}
