package ru.t1.shevyreva.tm.jms.listener;

import org.hibernate.event.spi.*;
import org.hibernate.persister.entity.EntityPersister;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.jms.operation.OperationEvent;
import ru.t1.shevyreva.tm.jms.operation.OperationType;

public class EntityListener implements PostInsertEventListener, PostUpdateEventListener, PostDeleteEventListener {

    @NotNull
    private JmsLoggerProducer jmsLoggerProducer;

    public EntityListener(final JmsLoggerProducer jmsLoggerProducer) {
        this.jmsLoggerProducer = jmsLoggerProducer;
    }

    @Override
    public void onPostDelete(PostDeleteEvent postDeleteEvent) {
        log(OperationType.DELETE, postDeleteEvent.getEntity());
    }

    @Override
    public void onPostInsert(PostInsertEvent postInsertEvent) {
        log(OperationType.INSERT, postInsertEvent.getEntity());
    }

    @Override
    public void onPostUpdate(PostUpdateEvent postUpdateEvent) {
        log(OperationType.UPDATE, postUpdateEvent.getEntity());
    }

    @Override
    public boolean requiresPostCommitHanding(EntityPersister entityPersister) {
        return false;
    }

    private void log(@NotNull final OperationType operationType, @NotNull final Object entity) {
        jmsLoggerProducer.send(new OperationEvent(operationType, entity));
    }

}
