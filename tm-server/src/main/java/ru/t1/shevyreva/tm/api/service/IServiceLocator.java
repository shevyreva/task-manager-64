package ru.t1.shevyreva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.service.dto.*;

public interface IServiceLocator {

    @NotNull
    IProjectDtoService getProjectService();

    @NotNull
    ITaskDtoService getTaskService();

    @NotNull
    IProjectTaskDtoService getProjectTaskService();

    /*@NotNull
    ILoggerService getLoggerService();*/


    @NotNull
    IUserDtoService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionDtoService getSessionService();

}
