package ru.t1.shevyreva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shevyreva.tm.dto.model.ProjectDTO;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectDtoRepository extends AbstractUserOwnedModelDtoRepository<ProjectDTO> {

    @Transactional
    void deleteByUserId(@NotNull final String userId);

    @Override
    @Transactional
    void deleteAll();

    @Override
    List<ProjectDTO> findAll();

    @Override
    Optional<ProjectDTO> findById(@NotNull final String id);

    List<ProjectDTO> findByUserId(@NotNull final String userId);

    @Nullable
    @Query("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY :sortType")
    List<ProjectDTO> findAllByUserIdWithSort(@Param("userId") String userId, @Param("sortType") String sortType);

    ProjectDTO findOneByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    ProjectDTO findOneById(@NotNull final String id);

    @Override
    long count();

    long countByUserId(@NotNull final String userId);

    @Transactional
    void deleteOneByIdAndUserId(@NotNull final String id, @NotNull final String userId);

    @Transactional
    void deleteOneById(@NotNull String id);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}
