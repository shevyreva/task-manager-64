package ru.t1.shevyreva.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.shevyreva.tm.dto.model.AbstractUserOwnedModelDTO;

@Repository
public interface AbstractUserOwnedModelDtoRepository<M extends AbstractUserOwnedModelDTO> extends AbstractModelDtoRepository<M> {

}
