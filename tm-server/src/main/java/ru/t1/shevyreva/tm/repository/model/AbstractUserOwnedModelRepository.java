package ru.t1.shevyreva.tm.repository.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.shevyreva.tm.model.AbstractUserOwnedModel;

@Repository
public interface AbstractUserOwnedModelRepository<M extends AbstractUserOwnedModel> extends AbstractModelRepository<M> {

}
