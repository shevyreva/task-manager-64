package ru.t1.shevyreva.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shevyreva.tm.api.service.model.ITaskService;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.enumerated.TaskSort;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.StatusNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.TaskNotFoundException;
import ru.t1.shevyreva.tm.exception.field.*;
import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.repository.model.TaskRepository;

import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
public final class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @NotNull
    @SneakyThrows
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(userId, task);
        return task;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Task create(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final Task task = new Task();
        task.setName(name);
        add(userId, task);
        return task;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);

        taskRepository.saveAndFlush(task);
        return task;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);

        taskRepository.saveAndFlush(task);
        return task;
    }

    @NotNull
    @SneakyThrows
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        return taskRepository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @Nullable
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId, @Nullable final TaskSort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        if (sort == null) return taskRepository.findByUserId(userId);
        if (sort != null) return taskRepository.findAllByUserIdWithSort(userId, sort.name());
        else return null;
    }

    @NotNull
    @SneakyThrows
    public Task findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable final Task task = taskRepository.findOneByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @NotNull
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        return taskRepository.countByUserId(userId);
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Task removeOne(@Nullable final String userId, @Nullable final Task task) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new ModelNotFoundException();

        taskRepository.deleteOneByIdAndUserId(userId, task.getId());
        return task;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Task removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        Task task = taskRepository.findOneByUserIdAndId(userId, id);
        taskRepository.findOneByUserIdAndId(userId, id);
        return task;
    }

    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        taskRepository.deleteByUserId(userId);
    }

    @SneakyThrows
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Collection<Task> add(@NotNull final Collection<Task> models) {
        if (models == null) throw new TaskNotFoundException();

        for (@NotNull Task task : models) {
            add(task);
        }
        return models;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Task add(@NotNull final Task model) {
        if (model == null) throw new TaskNotFoundException();

        taskRepository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Task add(@NotNull final String userId, @NotNull final Task model) {
        if (model == null) throw new ProjectNotFoundException();

        //model.setUserId(userId);
        taskRepository.saveAndFlush(model);
        return model;
    }

    @SneakyThrows
    @Transactional
    public void removeAll() {
        taskRepository.deleteAll();
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Collection<Task> set(@NotNull final Collection<Task> models) {
        @Nullable final Collection<Task> entities;
        removeAll();
        entities = add(models);
        return entities;
    }

    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return (taskRepository.findOneByUserIdAndId(userId, id) != null);
    }

}
