package ru.t1.shevyreva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shevyreva.tm.dto.model.TaskDTO;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskDtoRepository extends AbstractUserOwnedModelDtoRepository<TaskDTO> {

    @Transactional
    void deleteByUserId(@NotNull final String userId);

    @Override
    @Transactional
    void deleteAll();

    @Override
    List<TaskDTO> findAll();

    @Override
    Optional<TaskDTO> findById(@NotNull final String id);

    List<TaskDTO> findByUserId(@NotNull final String userId);

    @Nullable
    @Query("SELECT p FROM TaskDTO p WHERE p.userId = :userId ORDER BY :sortType")
    List<TaskDTO> findAllByUserIdWithSort(@Param("userId") String userId, @Param("sortType") String sortType);


    TaskDTO findOneByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    TaskDTO findOneById(@NotNull final String id);

    @Override
    long count();

    long countByUserId(@NotNull final String userId);

    @Transactional
    void deleteOneByIdAndUserId(@NotNull final String userId, @NotNull final String id);

    @Transactional
    void deleteOneById(@NotNull String id);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    List<TaskDTO> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

}
