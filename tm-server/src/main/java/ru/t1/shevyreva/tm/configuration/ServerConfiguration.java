package ru.t1.shevyreva.tm.configuration;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.t1.shevyreva.tm.api.service.IDatabaseProperty;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.t1.shevyreva.tm")
@EnableJpaRepositories("ru.t1.shevyreva.tm.repository")
public class ServerConfiguration {

    @NotNull
    @Autowired
    IDatabaseProperty propertyService;

    @Bean
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getDatabaseDriver());
        dataSource.setUrl(propertyService.getDatabaseUrl());
        dataSource.setUsername(propertyService.getDatabaseUsername());
        dataSource.setPassword(propertyService.getDatabasePassword());
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(
            final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            final DataSource dataSource
    ) {
        final LocalContainerEntityManagerFactoryBean factoryBean =
                new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.shevyreva.tm.dto", "ru.t1.shevyreva.tm.model");

        final Properties properties = new Properties();
        properties.put(Environment.DIALECT, propertyService.getDatabaseDialect());
        properties.put(Environment.HBM2DDL_AUTO, propertyService.getHBM2DDL());
        properties.put(Environment.SHOW_SQL, propertyService.getShowSQL());
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getUseSecondLevelCache());
        properties.put(Environment.USE_QUERY_CACHE, propertyService.getUseSecondLevelCache());
        properties.put(Environment.USE_MINIMAL_PUTS, propertyService.getUseMinimalPuts());
        properties.put(Environment.CACHE_REGION_FACTORY, propertyService.getCacheRegionFactory());
        properties.put(Environment.CACHE_REGION_PREFIX, propertyService.getCacheRegionPrefix());
        properties.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getCacheProviderConfig());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

}
