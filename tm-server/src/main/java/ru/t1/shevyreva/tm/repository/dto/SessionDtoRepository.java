package ru.t1.shevyreva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shevyreva.tm.dto.model.SessionDTO;

import java.util.List;
import java.util.Optional;

@Repository
public interface SessionDtoRepository extends AbstractUserOwnedModelDtoRepository<SessionDTO> {

    long count();

    @Transactional
    void deleteAll();

    @Override
    @Transactional
    void deleteById(@NotNull final String id);

    @Transactional
    void deleteByUserId(@NotNull final String userId);

    @NotNull
    List<SessionDTO> findAll();

    @NotNull
    @Override
    Optional<SessionDTO> findById(@NotNull final String id);

    @NotNull
    SessionDTO findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}
