<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>

<h1>TASK EDIT</h1>

<form:form action="/task/edit/${task.id}" method="POST" modelAttribute="task">
    <form:input type="hidden" path="id"/>
    <p>
    <div>NAME:</div>
    <div>
        <input type="text" name="name" value="${task.name}"/>
    </div>
    </p>

    <p>
    <div>DESCRIPTION:</div>
    <div>
        <form:input type="text" path="description"/>
    </div>
    </p>

    <p>
    <div>PROJECT:</div>
    <div>
        <form:select path="projectId">
            <form:option value="${null}" label="--- // ---"/>
            <form:options items="${projects}" itemLabel="name" itemValue="id"/>
        </form:select>
    </div>
    </p>

    <p>
    <div>STATUS:</div>
    <form:select path="status">
        <form:option value="${null}" label="--- // ---"/>
        <form:options items="${statuses}" itemLabel="displayName"/>
    </form:select>
    </p>

    <p>
    <div>DATE START:</div>
    <div>
        <form:input path="dateStart" type="date"/>
    </div>
    </p>

    <p>
    <div>DATE FINISH:</div>
    <div>
        <form:input path="dateFinish" type="date"/>
    </div>
    </p>

    <button type="submit">SAVE TASK</button>

</form:form>

<jsp:include page="../include/_footer.jsp"/>
